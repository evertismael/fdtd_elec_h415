% Validation scripts.
% Analize de recorded values of the Electric field ar the point
% (100,50);(101,50).
% a relative permitivity has been set in the region starting at 101,
% such that we have a change of material at the points 100 and 101 in the
% grid with a relative permitivity of 4.
% We compute the fourier transform of both points and take the ratio which
% has to be the same to the transmition coefitient.

%---------------------------------------------------------------------
%-------------- Include saved data and clean workspace --------------
%---------------------------------------------------------------------
clear all; close all; clc;
addpath(genpath('../classes'));
open('incident_2.mat');
incident = ans.obj;
clear ans
open('transmitted_21.mat');
transmitted = ans.obj;
%---------------------------------------------------------------------
%----------- compute the fourier transform --------------------
%---------------------------------------------------------------------

f_inc = fft(incident.ez(116:171,3));
f_inc = fftshift(f_inc);

f_trans = fft(transmitted.ez(116:171,4));
f_trans = fftshift(f_trans);


figure;
subplot(3,2,1)
stem(incident.ez(:,3));
grid;

subplot(3,2,2)
stem(abs(f_inc));
grid;

subplot(3,2,3)
stem(transmitted.ez(:,4));
grid;

subplot(3,2,4)
stem(abs(f_trans));
grid;

subplot(3,2,5)
stem(abs(f_trans)./abs(f_inc));
grid;
