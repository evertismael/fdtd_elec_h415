function value = ConfigReader(section,key)
     % ConfigReader  Reads a value from config file.
    ini = IniConfig();
    ini.ReadFile('config.ini');
    value = ini.GetValues(section, key);
    if isempty(value)
        error('EPC => Cannot find Key or section in config file.');
    end
    
    if isstring(value) || ischar(value)
        value = str2double(value);
    end
end