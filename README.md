# FDTD_ELEC_H415

The Finite-Difference Time-Domain (FDTD) method is widely used to numerically solve Maxwell�s equations. In this project, our objective is to develop a 2-dimensional (2D) FDTD code. This code will then be applied to problems theoretically tackled during the lectures. By performing numerical experiments, you are asked to think by yourselves about the concepts of wave propagation. 
To help you in doing the project, a reference is provided on the Virtual University : Understanding the Finite-Difference Time-Domain Method, John B. Schneider, 2015, that can be also freely downloaded from the web. Chapters 3 and 8 from this reference explain the ingredients required to fulfill the minimal goals of this project (see below). But it also allows you to go further.

## Getting Started

Just clone the repository and execute FdtdMain_XD.m in matlab.

### Some Comments about codign style

* Try to use the config file for global parameters, the correct way to read a parameter in ConfigFile is by using ConfigReader function defined in miscellaneous.
* Try to respect the structure of the proyect.
- **channel**: all the functions related to it like: losses, initialization, etc.
- **drawing**: helpers to visualize data.
- **updating**: functions to update electric and magnetic fields in 1 or x dimensions.
- **miscellaneous**: functions that can be used across the project.

## Authors

* Evert Ismael **Pocoma Copa**
* Juan Miguel **Fresneda**
* (Jerry White) Jieren **Wu**
