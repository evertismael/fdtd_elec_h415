classdef plotEz < handle
   properties
        Xaxis;
        Yaxis;
        
        Taxis;
        dt;
        dx;
        
        Movie;
        record;
        
        maxTimeStep;
        fig;
        
        dispFreq;
        
        % validation
        crosZero1 = [];
        crosZero2 = [];
        velocity =  [];
        
   end

   methods
    % --------------- Constructor -------------------
    function obj = plotEz(inGrid)
        obj.dispFreq = ConfigReader('other','dispFreq');
        dispScreen = ConfigReader('other','dispScreen');
        
        if(dispScreen) == 1
            obj.fig = figure('units','normalized','outerposition',[0 0 1 1]);
        else
            obj.fig = figure('units','normalized','outerposition',[0 0 1 1]);            
        end
        
        
        obj.Xaxis = (0:1:inGrid.NxNodes-1);%*inGrid.dx;
        obj.Yaxis = (0:1:inGrid.NyNodes-1);%*inGrid.dy;
        obj.Taxis = (0:inGrid.Ntsteps-1)*inGrid.dt;
        
        obj.maxTimeStep = inGrid.Ntsteps;
        obj.velocity = zeros(size(obj.Taxis));
        
        
        
        xlim([obj.Xaxis(1), obj.Xaxis(inGrid.NxNodes)]);
        ylim([obj.Yaxis(1), obj.Yaxis(inGrid.NyNodes)]);
        obj.dt = inGrid.dt;
        obj.dx = inGrid.dx;
    end
    
    % --------- display Data ----
    function obj = displayData(obj,timeStep,inGrid)
        if mod(timeStep,obj.dispFreq) == 0 || timeStep == 1
            subplot(2,4,[1 2 5 6])
            imagesc(obj.Xaxis,obj.Yaxis,inGrid.ez');
            axis equal tight;
            title(['timestep ',num2str(timeStep),' of ',num2str(obj.maxTimeStep)])
            xlabel('X grid in meters');
            ylabel('Y grid in meters');
            shading interp;
            colorbar
            axis tight
            caxis([-0.06 0.06])
            drawnow;
        end        
    end
    function obj = computeVelocity(obj,timeStep,dataSaver)
        if timeStep == 1
            return;
        end
        act1 = dataSaver.ez(timeStep,1);
        prev1 = dataSaver.ez(timeStep-1,1);

        act2 = dataSaver.ez(timeStep,2);
        prev2 = dataSaver.ez(timeStep-1,2);

        if prev1 < 0 && act1 > 0 || prev1 > 0 && act1 < 0
            if abs(prev1-act1)/2 > 0
                obj.crosZero1(end+1) = timeStep;
            else
                obj.crosZero1(end+1) = timeStep-1;
            end
        end
        if prev2 < 0 && act2 > 0 || prev2 > 0 && act2 < 0
            if abs(prev2-act2)/2 > 0
                obj.crosZero2(end+1) = timeStep;
            else
                obj.crosZero2(end+1) = timeStep-1;
            end
        end
    
        if length(obj.crosZero1)>0 && length(obj.crosZero2)>0  
            nn = min(length(obj.crosZero2),length(obj.crosZero1));
            x = dataSaver.index_xy(2,2) - dataSaver.index_xy(1,2) + 1;
            t = sum(obj.crosZero2(1:nn) - obj.crosZero1(1:nn))/nn;
            obj.velocity(timeStep) = (x*obj.dx)/(t*obj.dt);
        end
                
        if mod(timeStep,obj.dispFreq) == 0 || timeStep == 1 
            subplot(2,4,[3 4])
            plot(obj.Taxis',dataSaver.ez(:,1));
            grid;
            hold on;
            plot(obj.Taxis',dataSaver.ez(:,2));
            title(['Ez']);
            legend(['x = ',num2str(dataSaver.index_xy(1,1)),' y = ',num2str(dataSaver.index_xy(1,2))],...
                ['x = ',num2str(dataSaver.index_xy(2,1)),' y = ',num2str(dataSaver.index_xy(2,2))]);
            xlabel('time');
            ylabel('Ez');
            
            hold off;
            subplot(2,4,[7 8])
            plot(obj.Taxis,obj.velocity);
            grid;
            title(['experimental c0 = ', num2str(obj.velocity(timeStep),4),' [m/s]']);
            xlabel('time');
            ylabel('c0');
            drawnow; 
        end 
    end
    
    
    
    
    
    % --------- startRecording ----
    function obj = startMovie(obj)
        name = datestr(now,'mmddyyyy_HH_MM_SS');
        obj.Movie = VideoWriter(['output/',name],'MPEG-4');
        obj.Movie.FrameRate = 30;
        open(obj.Movie)
    end    
    % --------- stopRecording ----
    function obj = stop(obj)
        close(obj.Movie)
    end
    % --------- writeFrame ----
    function obj = writeFrame(obj,timeStep)
        if mod(timeStep,obj.dispFreq) == 0 || timeStep == 1
            F = getframe(obj.fig);
            writeVideo(obj.Movie,F);
        end
    end
   end
end