% Main script that execute all the simulation in 2 Dimension.
% Date: 16-04-18
% Only the case of TMz is taking into account.
% Magnetic fields is transversal to the normal of the propagation plain (z) 
% i.e. only Ez, Hy, Hx exist.

%---------------------------------------------------------------------
%-------------- Include directories and clean workspace --------------
%---------------------------------------------------------------------
clear all; close all; clc;
addpath(genpath('miscellaneous'));
addpath(genpath('drawing'));
addpath(genpath('classes'));
%---------------------------------------------------------------------
%----------- Load configParams and create objects --------------------
%---------------------------------------------------------------------
fdtdGrid = FdtdGrid();                          % create grid obj.
%---------------------------------------------------------------------
%------------------------ Creating the Yee Grid ----------------------
%---------------------------------------------------------------------
fdtdGrid = fdtdGrid.computeGridParameters();
fdtdGrid = fdtdGrid.initMatrices();
%fdtdGrid = fdtdGrid.setAttributeInZone('eps_r',4,[70,1,309,62]);    % other medium
fdtdGrid = fdtdGrid.computeUpdateConstants();
fdtdGrid = fdtdGrid.initABC();
sources = createSources(fdtdGrid);                      % create the sources.

%---------------------------------------------------------------------
%------------------------  Init Plotter and Saver of points ----------
%---------------------------------------------------------------------
dataSaver = DataSaver(fdtdGrid.Ntsteps);                 % create saver.
dataSaver = dataSaver.setPointsToSave([...      % save specific point values.
    25,40;
    25,140]);
plotter = plotEz(fdtdGrid);
plotter = plotter.startMovie();
%---------------------------------------------------------------------
%------------------------ Run simulation in time ---------------------
%---------------------------------------------------------------------


for timeStep=1:fdtdGrid.Ntsteps
    fdtdGrid = fdtdGrid.updateH();            % update magnetic field. 
    fdtdGrid = fdtdGrid.updateE();            % update electric field.
    fdtdGrid = fdtdGrid.addSource(sources,timeStep); 
    fdtdGrid = fdtdGrid.updateABC();
    
% --- PEC at the boundaries.
%     [ly,lx] = size(fdtdGrid.ez);
%     fdtdGrid.ez(:,1) = zeros(ly,1);
%     fdtdGrid.ez(:,end-1) = zeros(ly,1);
%     fdtdGrid.ez(1,:) = zeros(1,lx);
%     fdtdGrid.ez(end-1,:) = zeros(1,lx);
    
    % saving data and ploting
    dataSaver = dataSaver.savePoints(fdtdGrid,timeStep);    % record values.
    plotter.displayData(timeStep,fdtdGrid);
    plotter.computeVelocity(timeStep,dataSaver);
    plotter = plotter.writeFrame(timeStep);
end
dataSaver.saveMatFile();
plotter = plotter.stop();