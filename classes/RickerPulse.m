classdef RickerPulse < Source
   properties
      
   end
   methods
        % --------------- Constructor -------------------
    function obj = RickerPulse(index)
        obj.type = 'rPulse';
        obj.x = ConfigReader(['source_',num2str(index)],'x');  
        obj.y = ConfigReader(['source_',num2str(index)],'y');  
        obj.delay = ConfigReader(['source_',num2str(index)],'delay');  
        obj.width = 0.5/fmax;  
    end 

    function sourceValue = getIncidentField(obj,time,eps_r,mu_r)
        arg = pi*((Cdts*time - obj.delay)/obj.ppw -1.0);
        arg = arg*arg;
        sourceValue = (1.0 - 2.0*arg)*exp(-arg);
        
        sourceValue = exp(-((time*obj.deltaT - 2*obj.width)/obj.width).^2)
        
        
    end
   end
end 