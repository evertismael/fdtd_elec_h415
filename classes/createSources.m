function sources = createSources(fdtdGrid)
    % 0:rpulse,1:gpulse,2:sin 
    ini = IniConfig();
    ini.ReadFile('config.ini');
    sections = ini.GetSections();
    myindices = ~cellfun(@isempty,regexp(sections,'source_*'));
    sections = sections(myindices);
    sources = [];
    i=1;
    type = ConfigReader(sections{i},'type');
    if type == 0           
        source = RickerPulse(i,fdtdGrid);
    elseif type == 1   
        source = GaussianPulse(i,fdtdGrid);
    elseif type == 2   
        source = Harmonic(i,fdtdGrid);
    else
        error('Source not supported');
    end
    sources{i} = source;
end