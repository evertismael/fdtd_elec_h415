classdef Source < handle
   % Abstract class for differnt types of sources. 
   properties
      x;
      y;
      delay;
      type;
      deltaT;
   end
   methods (Abstract)
      getIncidentField(obj,time,eps_r,mu_r)
   end
end 