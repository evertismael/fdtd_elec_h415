classdef Harmonic < Source
   properties
       f
       smoth
   end
   methods
        % --------------- Constructor -------------------
    function obj = Harmonic(index,fdtdGrid)
        fmax = ConfigReader('grid','fmax');
        obj.type = 'harmonic';
        obj.x = ConfigReader(['source_',num2str(index)],'x');  
        obj.y = ConfigReader(['source_',num2str(index)],'y');  
        obj.delay = ConfigReader(['source_',num2str(index)],'delay');
        obj.deltaT = fdtdGrid.dt;
        obj.f = fmax/2;
        obj.smoth = ones(fdtdGrid.Ntsteps,1);
        time = 1:2*obj.delay;
        f1 = 1/(2*2*obj.delay);
        w = 2*pi*f1;
        obj.smoth(1:2*obj.delay) = 0.5*(1 + cos(w.*time + pi));        
    end 

    function sourceValue = getIncidentField(obj,time,eps_r,mu_r)
        w = 2*pi*obj.f*obj.deltaT;
        sourceValue = obj.smoth(time)*cos(w*time);
    end
   end
end 