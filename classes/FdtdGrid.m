classdef FdtdGrid < handle
% The grid, containing all the magnetic and electric fields. It also
% implements the methods to updates the fields.
   properties
        dt;                         % delta time.
        dx;                         % delta x.
        dy;                         % delta y.
        
        Ntsteps;                     % Number of time steps.
        NxNodes;                    % number of Nodes in X;
        NyNodes;                    % number of Nodes in Y;
        
        ez;hx;hy;                   % magnetic and electric fields
        eps_r; mu_r;                % inhomogenities. 
        sigma;                      % rel permitivity/permeability and conductivity
        
        chxe;chye;                  % coefitients of equations: 
        ceze;cezh;                  % chxe: constant updating hx affecting e.
        
        ezLeft;ezRight;             % memory for Absorbing boundary conditions
        ezTop;ezBottom;
        coef0;coef1;coef2;          % coefitients to update ABC.
   end

   methods
    % --------------- Constructor -------------------
    function obj = FdtdGrid()
        
    end
    % ---------
    function obj = computeGridParameters(obj)
        % compute deltaX and deltaY.
        fmax = ConfigReader('grid','fmax');
        Nlamb0 = ConfigReader('grid','Nlamb0');
        Nd = ConfigReader('grid','Nd');
        c0 = ConfigReader('constants','c0');
        Nlossy = ConfigReader('grid','Nlossy');
        
        
        n_max = 1;                  % TODO: change for max refractive index.
        lamb_min = c0/(n_max*fmax);
        delta_lamb_min = lamb_min/Nlamb0;
        
        d = 100000;                    % TODO: change for min structure distance (based in devises)
        delta_d = d/Nd;
        
        obj.dx = min(delta_lamb_min,delta_d);
        obj.dy = obj.dx;
        
        % compute delta time.
        courant = 0.25; %1/sqrt(2);
        temp_dt = obj.dx/((1/courant)*c0);
        %Nt = ceil(1/(fmax*temp_dt));     % integer numbers of min wavelength in grid.
        %obj.dt = 1/(fmax*Nt);
        obj.dt = temp_dt;
        % total simulation time and Ntsteps.
        Lx = ConfigReader('grid','Lx');
        Ly = ConfigReader('grid','Ly');
        d = sqrt(Lx^2 + Ly^2);
        Tprop = d/(c0/n_max);
        Tsim = 6*Tprop;
        obj.Ntsteps = ceil(Tsim/obj.dt);
        
        % number of nodes.
        obj.NxNodes = ceil(Lx/obj.dx) + Nlossy*2;
        obj.NyNodes = ceil(Ly/obj.dy) + Nlossy*2;
        
        % display params.
        disp('==========FDTD-Grid parameters==========');
        disp(['fmax = ',num2str(fmax,2),' [Hz]']); 
        disp(['minLamb = ',num2str(lamb_min,2),' [m]']);         
        disp(['dt = ',num2str(obj.dt,2),' [sec]']);
        disp(['dx = ',num2str(obj.dx,2),' [m]']);
        disp(['dy = ',num2str(obj.dy,2),' [m]']);
        disp(['courant Number = ',num2str(courant,2)]);
        disp(['Ntsteps = ',num2str(obj.Ntsteps,3)]);
        disp(['NxNodes = ',num2str(obj.NxNodes,3)]);
        disp(['NyNodes = ',num2str(obj.NyNodes,3)]);
    end
    
    %--------- Init the matrices
    function obj = initMatrices(obj)
         Nx = obj.NxNodes;
         Ny = obj.NyNodes;
         
         obj.ez = zeros(Nx,Ny);
         obj.hx = zeros(Nx,Ny);
         obj.hy = zeros(Nx,Ny);
         
         % inhomogenities.
         obj.eps_r = ones(Nx,Ny);
         obj.mu_r = ones(Nx,Ny);
         obj.sigma = zeros(Nx,Ny);
         
         % update coefitients
         obj.ceze = zeros(Nx,Ny);
         obj.cezh = zeros(Nx,Ny);
         obj.chxe = zeros(Nx,Ny);
         obj.chye = zeros(Nx,Ny);
         
         
         
         % TODO: put the coefitients of the devices defined.
         
    end
    
    %----- Init ABC -------
    function obj = initABC(obj)
        Nx = obj.NxNodes;
        Ny = obj.NyNodes;
        
        obj.ezLeft = zeros(3,Ny,2); 
        obj.ezRight = zeros(3,Ny,2);
        obj.ezTop = zeros(Nx,3,2);
        obj.ezBottom = zeros(Nx,3,2);
        % coefitients to update the previous matrices.
        temp1 = sqrt(obj.cezh(1,1)*obj.chye(1,1));
        temp2 = 1.0/temp1 + 2.0 + temp1;
        obj.coef0 = -(1.0 / temp1 -2.0 + temp1)/temp2;
        obj.coef1 = -2.0 * (temp1 -1.0 / temp1)/temp2;
        obj.coef2 = 4.0 * (temp1 + 1.0 /temp1)/temp2;
    end
    
    %----- Compute update constants -------
    function obj = computeUpdateConstants(obj)
        Nx = obj.NxNodes;
        Ny = obj.NyNodes;
         
        imp0 = ConfigReader('constants','imp0');
        eps0 = ConfigReader('constants','eps0');
        c0 = ConfigReader('constants','c0');
        
        Nlossy = ConfigReader('grid','Nlossy');
        
        % --------------------------------------------------
        % --------- Lossy Boundary constants - PML
        tmp = eps0/(2*obj.dt)*0.00;
        for nx = 1:Nlossy
            obj.sigma(nx,:) = tmp*((Nlossy-nx)/Nlossy)*ones(size(obj.sigma(nx,:))); 
            obj.sigma(end-nx+1,:) = tmp*((nx)/Nlossy)*ones(size(obj.sigma(end-nx+1,:))); 
        end
        for ny = 1:Nlossy
            obj.sigma(:,ny) = tmp*((Nlossy-ny)/Nlossy)*ones(size(obj.sigma(:,ny))); 
            obj.sigma(:,end-ny+1) = tmp*((ny)/Nlossy)*ones(size(obj.sigma(:,end-ny+1))); 
        end
        
        
        % --------------------------------------------------
        % Ez field constants.
        for nx = 1:1:Nx
            for ny = 1:1:Ny
                temp = (obj.sigma(nx,ny)*imp0*c0*obj.dt)/(2*obj.eps_r(nx,ny));
                temp2 = (c0*obj.dt)/(obj.eps_r(nx,ny)*obj.dx);
                
                obj.ceze(nx,ny) = (1-temp)/(1+temp);
                obj.cezh(nx,ny) = (temp2)/(1+temp);
            end
        end
        
        % Hx field constants.
        for nx = 1:1:Nx
            for ny = 1:1:Ny
                temp = (c0*obj.dt)/(obj.mu_r(nx,ny)*obj.dx);
                obj.chxe(nx,ny) = temp;
            end
        end
        
        % Hy field constants.
        for nx = 1:1:Nx
            for ny = 1:1:Ny
                temp = (c0*obj.dt)/(obj.mu_r(nx,ny)*obj.dx);
                obj.chye(nx,ny) = temp;
            end
        end    
    end
    
    % ---- updateH ----------------
    function obj = updateH(obj)
        Nx = obj.NxNodes;
        Ny = obj.NyNodes;
        
        % update Hx
        for nx = 1:1:Nx-1  
            for ny = 1:1:Ny-1  
                obj.hx(nx,ny) = obj.hx(nx,ny)...
                    - obj.chxe(nx,ny)*( obj.ez(nx,ny+1)-obj.ez(nx,ny) );
            end
        end
        
        % updateHy
        for nx = 1:1:Nx-1 
            for ny = 1:1:Ny-1
                obj.hy(nx,ny) = obj.hy(nx,ny)...
                    + obj.chye(nx,ny)*( obj.ez(nx+1,ny)-obj.ez(nx,ny) );
            end
        end
    end
    
    % ---- updateE ----------------
    function obj = updateE(obj)
        Nx = obj.NxNodes;
        Ny = obj.NyNodes;
        
        for nx = 2:1:Nx-1  
            for ny = 2:1:Ny-1 
                obj.ez(nx,ny)=obj.ceze(nx,ny)*obj.ez(nx,ny)...
                    + obj.cezh(nx,ny)*( (obj.hy(nx,ny)-obj.hy(nx-1,ny))...
                                       -(obj.hx(nx,ny)-obj.hx(nx,ny-1)) );
            end
        end        
    end
    
    % ---- addSource ----------------
    function obj = addSource(obj, sources,time)
        for i=1:length(sources)
            s = sources{i};
            incidentField =  s.getIncidentField(time,obj.eps_r,obj.mu_r);
            obj.ez(s.x,s.y) = obj.ez(s.x,s.y) + incidentField;
        end
    end    
    
    % -----updateABC ---------------
    function obj = updateABC(obj)
        Nx = obj.NxNodes;
        Ny = obj.NyNodes;
        for ny = 1:Ny
            %----left
            obj.ez(1,ny) = obj.coef0 * (obj.ez(3,ny) + obj.ezLeft(1,ny,2))...
                + obj.coef1 * (obj.ezLeft(1,ny,1) + obj.ezLeft(3,ny,1)...
                                  - obj.ez(2,ny) - obj.ezLeft(2,ny,2))...
                + obj.coef2 * obj.ezLeft(2,ny,1) - obj.ezLeft(3,ny,2);
            %----right
            obj.ez(end,ny) = obj.coef0 * (obj.ez(end-2,ny) + obj.ezRight(1,ny,2))...
                + obj.coef1 * (obj.ezRight(1,ny,1) + obj.ezRight(3,ny,1)...
                                  - obj.ez(end-1,ny) - obj.ezRight(2,ny,2))...
                + obj.coef2 * obj.ezRight(2,ny,1) - obj.ezRight(3,ny,2);
            %-- memorize old fields.
            for nx=1:3
                obj.ezLeft(nx,ny,2) = obj.ezLeft(nx,ny,1);
                obj.ezLeft(nx,ny,1) = obj.ez(nx,ny);
                obj.ezRight(nx,ny,2) = obj.ezRight(nx,ny,1);
                obj.ezRight(nx,ny,1) = obj.ez(end-nx+1,ny);
            end

            
        end
        %-------- ABC top-bottom
        for nx = 1:Nx
            %---- bottom
            obj.ez(nx,1) = obj.coef0 * (obj.ez(nx,3) + obj.ezBottom(nx,1,2))...
                + obj.coef1 * (obj.ezBottom(nx,1,1) + obj.ezBottom(nx,3,1)...
                            - obj.ez(nx,2) - obj.ezBottom(nx,2,2))...
                + obj.coef2 * obj.ezBottom(nx,2,1) - obj.ezBottom(nx,3,2);
            %---- top
            obj.ez(nx,end) = obj.coef0 * (obj.ez(nx,end-2) + obj.ezTop(nx,1,2))...
                + obj.coef1 * (obj.ezTop(nx,1,1) + obj.ezTop(nx,3,1)...
                            - obj.ez(nx,end-1) - obj.ezTop(nx,2,2))...
                + obj.coef2 * obj.ezTop(nx,2,1) - obj.ezTop(nx,3,2);

            %-- memorize old fileds.
            for ny = 1:3
                obj.ezBottom(nx,ny,2) = obj.ezBottom(nx,ny,1);
                obj.ezBottom(nx,ny,1) = obj.ez(nx,ny);
                obj.ezTop(nx,ny,2) = obj.ezTop(nx,ny,1);
                obj.ezTop(nx,ny,1) = obj.ez(nx,end-ny+1);
            end
        end
    end
    
    % -----set attribute zone ---------------
    function obj = setAttributeInZone(obj,attribute,value,rectP1P2)
        x1 = rectP1P2(1);
        y1 = rectP1P2(2);
        x2 = rectP1P2(3);
        y2 = rectP1P2(4);
        
        switch attribute
           case 'eps_r'
              obj.eps_r(x1:x2,y1:y2) = value;
           case 'mu_r'
              obj.mu_r(x1:x2,y1:y2) = value;
           case 'loss'
              obj.loss(x1:x2,y1:y2) = value;
           otherwise
              error('parameter not supported in channel');
        end
    end
   end
end