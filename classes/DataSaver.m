classdef DataSaver < handle
   % Abstract class for differnt types of sources. 
   properties
        index_xy;           % coordinates of fields to save values.
        max_time;           % duration of simulation;
        ez;                 % magnetic and electric fields
        eps_r; mu_r;        % inhomogenities. 
        sigma;
   end
   methods
       function obj = DataSaver(time_duration)
           obj.max_time = time_duration;
       end
       function obj = setPointsToSave(obj,index_xy)
            obj.index_xy = index_xy;
            [n_data,~] = size(index_xy);
            obj.ez = zeros(obj.max_time,n_data);
            obj.eps_r = zeros(obj.max_time,n_data);
            obj.mu_r = zeros(obj.max_time,n_data);
            obj.sigma = zeros(obj.max_time,n_data);
       end
       function obj = savePoints(obj,inGrid,time)
           [n_data,~] = size(obj.index_xy);
           for i=1:n_data
               x = obj.index_xy(i,1);
               y = obj.index_xy(i,2);
               obj.ez(time,i) = inGrid.ez(x,y);
               obj.eps_r(time,i) = inGrid.eps_r(x,y);
               obj.mu_r(time,i) = inGrid.mu_r(x,y);
               obj.sigma(time,i) = inGrid.sigma(x,y);       
           end
       end
       function [] = saveMatFile(obj)
            ez = obj.ez;
            index_xy = obj.index_xy;
            max_time = obj.max_time;
            eps_r = obj.eps_r; 
            mu_r = obj.mu_r;
            sigma = obj.sigma;
            name = datestr(now,'mmddyyyy_HH_MM_SS');
            save(['output/',name,'savedPoints.mat'],'obj');
       end
   end
end 