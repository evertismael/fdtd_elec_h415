classdef GaussianPulse < Source
   properties
      width;
   end
   methods
        % --------------- Constructor -------------------
    function obj = GaussianPulse(index,fdtdGrid)
        fmax = ConfigReader('grid','fmax');
        
        obj.type = 'gPulse';
        obj.x = ConfigReader(['source_',num2str(index)],'x');  
        obj.y = ConfigReader(['source_',num2str(index)],'y');  
        obj.deltaT = fdtdGrid.dt;
        obj.delay = ConfigReader(['source_',num2str(index)],'delay');  
        obj.width = 0.5/fmax;        
    end 

    function sourceValue = getIncidentField(obj,time,eps_r,mu_r)
        sourceValue = exp(-((time*obj.deltaT - 2*obj.width)/obj.width).^2);
    end
   end
end 