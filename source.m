%---------------------------------------------------------------------
%-------------- Include directories and clean workspace --------------
%---------------------------------------------------------------------
clear all; close all; clc;
addpath(genpath('miscellaneous'));
addpath(genpath('output'));
addpath(genpath('classes'));
load('05132018_00_59_35savedPoints.mat');
data = obj;
clear obj;

plot(data.ez)