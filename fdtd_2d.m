% Main script that execute all the simulation in 2 Dimension.
% Date: 16-04-18
% Only the case of TMz is taking into account.
% Magnetic fields is transversal to the normal of the propagation plain (z) 
% i.e. only Ez, Hy, Hx exist.

%---------------------------------------------------------------------
%-------------- Include directories and clean workspace --------------
%---------------------------------------------------------------------
clear all; close all; clc;
addpath(genpath('miscellaneous'));
addpath(genpath('drawing'));
addpath(genpath('classes'));
%---------------------------------------------------------------------
%----------- Load configParams and create objects --------------------
%---------------------------------------------------------------------
maxTime = ConfigReader('general','maxTime');    % simulation time
plotter = plotEz();
fdtdGrid = FdtdGrid();                          % create grid obj.
sources = createSources();                      % create the sources.
dataSaver = DataSaver(maxTime);                 % create saver.
dataSaver = dataSaver.setPointsToSave([...      % save specific point values.
    20,20;
    98,20;
    50,20;
    20,50;]);
%---------------------------------------------------------------------
%--- Configure lossy and / or inHomogenities zones in the channel ----
%---------------------------------------------------------------------

%fdtdGrid = fdtdGrid.setAttributeInZone('loss',0.02,[10,20,20,30]);    % other medium

%fdtdGrid = fdtdGrid.setAttributeInZone('eps_r',4,[101,1,200,200]);    % other medium

%---------------------------------------------------------------------
%------------------------ Pre-compute coefitients --------------------
%---------------------------------------------------------------------

fdtdGrid = fdtdGrid.computeUpdateConstants();
fdtdGrid = fdtdGrid.initABC();
%---------------------------------------------------------------------
%------------------------ Run simulation in time ---------------------
%---------------------------------------------------------------------
plotter = plotter.start();


for time=1:maxTime
    time;
    fdtdGrid = fdtdGrid.updateH();            % update magnetic field. 
    fdtdGrid = fdtdGrid.updateE();            % update electric field.
    fdtdGrid = fdtdGrid.addSource(sources,time); 
    fdtdGrid = fdtdGrid.updateABC();
    
    %--- PEC at the boundaries.
%     [ly,lx] = size(fdtdGrid.ez);
%     fdtdGrid.ez(:,1) = zeros(ly,1);
%     fdtdGrid.ez(:,end-1) = zeros(ly,1);
%     fdtdGrid.ez(1,:) = zeros(1,lx);
%     fdtdGrid.ez(end-1,:) = zeros(1,lx);
%     
    
    dataSaver = dataSaver.savePoints(fdtdGrid,time);    % record values.
    plotter = plotter.writeFrame(fdtdGrid);
    
    % iteration control.
    if mod(time,30) == 0
        disp(['computing for time = ',num2str(time)]);
    end
end
dataSaver.saveMatFile();
plotter = plotter.stop();